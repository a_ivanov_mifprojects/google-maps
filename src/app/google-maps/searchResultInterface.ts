import Autocomplete = google.maps.places.Autocomplete;
import InfoWindow = google.maps.InfoWindow;

export interface SearchResult {
  autocomplete: Autocomplete;
  infowindow: InfoWindow;
}
