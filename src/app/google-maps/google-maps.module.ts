import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GoogleMapsRoutingModule} from './google-maps-routing.module';
import {MapComponent} from './map/map.component';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    GoogleMapsRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule
  ]
})
export class GoogleMapsModule {
}
