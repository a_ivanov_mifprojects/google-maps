import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserPosition} from '../userPositionInterface';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('infoWindowContent') infowindowContent: ElementRef;
  @ViewChild('pacCard') pacCard: ElementRef;
  @ViewChild('pacInput') pacInput: ElementRef;
  @ViewChild('placeIcon') placeIcon: ElementRef;
  @ViewChild('placeName') placeName: ElementRef;
  @ViewChild('placeAddress') placeAddress: ElementRef;

  map: google.maps.Map;
  marker: google.maps.Marker;
  infowindow: google.maps.InfoWindow;

  constructor(private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.setUserPosition();
    this.setAutocomplete();
  }

  setUserPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        this.setMap({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      });
      return;
    }
    this.setMap({lat: 50.4501, lng: 30.5234});
  }

  setMap(position: UserPosition) {
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: position,
      zoom: 12
    });
    this.setMarker();
    this.setInfowindow();
    this.checkPlaceIdQuery();
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.pacCard.nativeElement);

  }

  setInfowindow() {
    this.infowindow = new google.maps.InfoWindow();
    this.infowindow.setContent(this.infowindowContent.nativeElement);
  }

  checkPlaceIdQuery() {
    if (typeof this.route.snapshot.queryParams.placeId !== 'undefined') {
      this.geocoding(this.route.snapshot.queryParams.placeId);
    }
  }

  geocoding(placeId: string) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({placeId}, (results) => {
      this.setPlace(this.convertGeocoderResult(results[0]));
    });
  }

  setPlace(place: google.maps.places.PlaceResult) {
    this.infowindow.close();
    this.marker.setVisible(false);
    this.map.setCenter(place.geometry.location);
    this.map.setZoom(12);
    this.marker.setPosition(place.geometry.location);
    this.marker.setVisible(true);
    this.placeIcon.nativeElement.src = place.icon;
    this.placeName.nativeElement.textContent = place.name;
    this.placeAddress.nativeElement.textContent = this.setAddress(place);
    this.infowindow.open(this.map, this.marker);
    this.setPlaceIdQuery(place.place_id);
  }

  convertGeocoderResult(geocoderResult: google.maps.GeocoderResult) {
    const place: google.maps.places.PlaceResult = {
      address_components: geocoderResult.address_components,
      formatted_address: geocoderResult.formatted_address,
      geometry: geocoderResult.geometry,
      icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
      name: geocoderResult.address_components[0].short_name,
      place_id: geocoderResult.place_id
    };
    return place;
  }

  setMarker() {
    this.marker = new google.maps.Marker({
      animation: undefined,
      clickable: false,
      cursor: '',
      draggable: false,
      icon: undefined,
      label: undefined,
      opacity: 0,
      optimized: false,
      place: undefined,
      position: undefined,
      shape: undefined,
      title: '',
      visible: false,
      zIndex: 0,
      map: this.map,
      anchorPoint: new google.maps.Point(0, 0)
    });
  }

  setAddress(place: google.maps.places.PlaceResult) {
    return [
      (place.address_components[0] && place.address_components[0].short_name || ''),
      (place.address_components[1] && place.address_components[1].short_name || ''),
      (place.address_components[2] && place.address_components[2].short_name || '')
    ].join(' ');
  }

  setAutocomplete() {
    const autocomplete: google.maps.places.Autocomplete = new google.maps.places.Autocomplete(this.pacInput.nativeElement);
    autocomplete.addListener('place_changed', () => {
      this.setPlace(autocomplete.getPlace());
    });
  }

  setPlaceIdQuery(placeId: string) {
    this.router.navigate([], {
      queryParams: {
        placeId
      }
    });
  }
}

