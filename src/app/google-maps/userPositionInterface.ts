export interface UserPosition {
  lat: number;
  lng: number;
}
